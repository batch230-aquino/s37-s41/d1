const mongoose = require("mongoose");
const Course = require("../models/course");

//function for adding a course
// 2. update the "addCourse" controller method to implement authentication for creating a course.
/*
module.exports.addCourse = (reqBody) => {

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })

    return newCourse.save().then((newCourse, error) => {
        
        if(error){
            return error;
        }
        else{
            return newCourse;
        }
    })
}
*/
// all course
module.exports.getAllCourse = () =>{
    return Course.find().then(result => {
        return result;
    })
}
// get active course
module.exports.getActiveCourse = () =>{
    return Course.find({isActive: true}).then(result => {
        return result;
    })
}
//specific course
module.exports.getCourse = (courseId) =>{
    return Course.findById(courseId).then(result => {
        return result;
    })
}

//UPDATING a course

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
                
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//adding a course/debug later

// module.exports.addCourse = (courseId, newCourse) => {
// 	newCourse.isAdmin = true;
// 	if(newCourse.isAdmin == true){
// 		return Course.findById(courseId,
// 			{
// 				// newData.course.name
// 				// newData.request.body.name
// 				name: newCourse.course.name, 
// 				description: newCourse.course.description,
// 				price: newCourse.course.price,
// 				slots:newCourse.course.slots
                
// 			}
// 		).then((result, error)=>{
// 			if(error){
// 				return false;
// 			}
// 			return true
// 		})
// 	}
// 	else{
// 		let message = Promise.resolve('User must be ADMIN to access this functionality');
// 		return message.then((value) => {return value});
// 	}
// }

module.exports.addCourse = (reqBody, result) => {
	if(result.isAdmin == true){
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((newCourse, error) =>
		{
			if (error){
				return error;		
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//Activity
module.exports.courseIdToArchive = (courseId, archiveCourse) => {
	if(archiveCourse.isAdmin == true){
	return Course.findByIdAndUpdate(courseId,
	{
		isActive: archiveCourse.course.isActive
	})
	.then((result, error)=>{
		if(error){

			return false;
		} 
		else{
			return true;
		}
	})

	.catch((err) => {

	console.log('Error occured', err);
	});
	}
	else{

	let message = Promise.resolve('User must be ADMIN to access this functionality');
	return message.then((value) => {return value});
	}
}