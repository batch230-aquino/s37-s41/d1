const User = require("../models/User.js");
const Course = require("../models/course");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const { response } = require("express");


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
		// hashing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}


module.exports.checkEmailExist = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); //true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;

			}
		}
	})
}

// s38 Activity
// module.exports.getProfile = (reqBody) => {
//     return User.findById(reqBody._id).then( (result, error)=> {
        
// 		if (error) {
//             return false;
//         } 
		
// 		else {
// 			result.password = "*****";
//             return result;
//         }
//     });
// }
// end of activity

//s41
module.exports.getProfile2 = (request, response) => {
	//will contain your token
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {

		result.password = "*****";
		response.send(result);
	})
}

// enroll feature
module.exports.enroll = async (request, response) =>{

	const userData = auth.decode(request.headers.authorization);

	let courseName = await Course.findById(request.body.courseId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved form the request body
		courseId:request.body.courseId,
		// courseName value is retrieved using findById method.
		courseName: courseName
	}

	console.log(newData);

		// a user is updated if we receive a "true" value
	let isUserUpdated = await User.findById(newData.userId)
	.then(user =>{
		user.enrollment.push({
			courseId: newData.courseId,
			courseName: newData.courseName
		});

		// Save the updated user information in the database
		return user.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(newData.courseId).then(course =>{

		course.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
		g
		// Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a response that a user is not allowed to enroll due to no available slots
		// [3] Else if the slot is negative value, it should make the slot value to zero


		// Check if the slot availble is 0
		if (course.slots === 0) { 

			console.log("Not allowed to Enroll due to No Available Slots")
			return false;
		}
		
		else if (course.slots < 0) { 
			course.slots = 0;
		}
		// Minus the slots available by 1
		course.slots -= 1;

		return course.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isCourseUpdated);

	// Condition will check if the both "user" and "course" document have been updated.
	// ternary operator
	(isUserUpdated && isCourseUpdated) ? response.send(true) : response.send(false)

}



	
		


