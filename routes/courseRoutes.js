
const express = require ("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

//Activity
/*
    1. Refractor the "course" route to implement "user authentication for the admin" when creating a course

    note:
    include screenshot of successful addcourse and not successful
*/

router.post("/create", (request, response) => {
    courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
})

router.get("/all", (request, response) => {
    courseControllers.getAllCourse(request.body).then(resultFromController => response.send(resultFromController))
})

router.get("/active", (request, response) => {
    courseControllers.getActiveCourse(request.body).then(resultFromController => response.send(resultFromController))
})

router.get("/:courseId", (request, response) => {
    courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
})

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})
// task for later debug
// router.post("/:courseId/addCourse", auth.verify, (request,response) => 
// {
// 	const newCourse = {
// 		course: request.body, 	//request.headers.authorization contains jwt
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}

// 	courseControllers.addCourse(request.body, newCourse).then(resultFromController => {
// 		response.send(resultFromController)
// 	})
// })

router.post("/add", auth.verify, (request, response) => 
{
		const result = {
			course: request.body, 	
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
	courseControllers.addCourse(request.body, result).then(resultFromController => response.send(resultFromController));
})

router.patch("/:courseId/archive", auth.verify, (request, response) => 
{
	const archiveCourse = {
		course: request.body, 	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.courseIdToArchive(request.params.courseId, archiveCourse).then(resultFromController => response.send(resultFromController));
}) 


module.exports = router;