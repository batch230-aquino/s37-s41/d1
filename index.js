
const express = require ("express");
const mongoose = require ("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

 // to create a express server/applications
 const app = express();
 
 //Middlewares - allows to bridge our backend application (server) tour front end
 // to allow corss origin resources sharing

 app.use(cors());

 //to read json objects
 app.use(express.json());

 // to read forms
 app.use(express.urlencoded({extended:true}));

 app.use("/users", userRoutes);
 app.use("/courses", courseRoutes)

 //connect to our mongoDB database
 mongoose.connect("mongodb+srv://admin:admin@batch230.vsenssp.mongodb.net/courseBooking?retryWrites=true&w=majority",
 {
    useNewUrlParser: true,
    useUnifiedTopology: true
 })

 mongoose.connection.once("open", () => console.log("Now connected to Aquino-Mongo DB Atlas"));

 app.listen(process.env.PORT || 4000, () =>
    {console.log(`API is now online on port ${process.env.PORT || 4000}`)}
 );
